<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Task;
use App\Models\Client;
use App\Models\State;
use App\Models\TaskType;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $tasks = Task::where('user_id', Auth::user()->id)->where('expiration_date', '>', date('Y-m-d'))->where('completed', '=', 0)->orderBy('name')->get();
        $expiredTasks = Task::where('user_id', Auth::user()->id)->where('expiration_date', '<',  date('Y-m-d'))->where('completed', '=',  0)->orderBy('name')->get();
        $completedTasks = Task::where('user_id', Auth::user()->id)->where('completed', '=',  1)->orderBy('name')->get();
        $types = TaskType::all();
        $clients = Client::all();
        $states = State::all();
        $users = User::all();
        return view('pages.tasks.index', [
            'tasks' => $tasks,
            'expiredTasks' => $expiredTasks,
            'completedTasks' => $completedTasks,
            'types' => $types,
            'clients' => $clients,
            'states' => $states,
            'users' => $users,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $types = TaskType::all();
        $clients = Client::all();
        $states = State::all();
        $users = User::all();
        return view('pages.tasks.create',
        [
            'types' => $types,
            'clients' => $clients,
            'states' => $states,
            'users' => $users,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $form_data = $request->all();
        $form_data['user_id'] = Auth::user()->id;
        $new_task = new Task();
        $new_task->fill($form_data);
        $new_task->completed = 0;
        $new_task->task_type_id = $form_data['task_type_id'];
        $new_task->client_id = $form_data['client_id'];
        $new_task->state_id = $form_data['state_id'];
        $new_task->assigned_user_id = $form_data['assigned_user_id'];
        $new_task->save();

        return redirect()->route('tasks.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Task $task)
    {
        $types = TaskType::all();
        $clients = Client::all();
        $states = State::all();
        $users = User::all();
        return view('pages.tasks.show', [
            'task' => $task,
            'types' => $types,
            'clients' => $clients,
            'states' => $states,
            'users' => $users,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Task $task)
    {

        $types = TaskType::all();
        $clients = Client::all();
        $states = State::all();
        $users = User::all();
        return view('pages.tasks.edit', [
            'task' => $task,
            'types' => $types,
            'clients' => $clients,
            'states' => $states,
            'users' => $users,
       ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Task $task)
    {
        $form_data = $request->all();
        $task->completed = 0;
        $task->update($form_data);
        return redirect()->route('tasks.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Task $task)
    {
       $task->delete();
       return redirect()->route('tasks.index')->with('message', "$task->name è stato eliminato");
    }

    public function completed(Request $request, Task $task)
    {
        $form_data = $request->all();
        $task->completed = 1;
        $task->update($form_data);
        return redirect()->back();
    }
    public function notCompleted(Request $request, Task $task)
    {
        $form_data = $request->all();
        $task->completed = 0;
        $task->update($form_data);
        return redirect()->route('tasks.index');
    }

}
