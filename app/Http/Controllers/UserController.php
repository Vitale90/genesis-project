<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Task;
use App\Models\TaskType;
use App\Models\Client;
use App\Models\State;
use Illuminate\Support\Facades\Auth;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users = User::all();
        return view('pages.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user)
{
    $user = User::where('id', Auth::user()->id)->get();
    $activeTasks = Task::where('assigned_user_id', Auth::user()->id)->where('expiration_date', '>', date('Y-m-d'))->where('completed', '=', 0)->orderBy('name')->get();
    $expiredTasks = Task::where('assigned_user_id', Auth::user()->id)->where('expiration_date', '<',  date('Y-m-d'))->where('completed', '=',  0)->orderBy('name')->get();
    $completedTasks = Task::where('assigned_user_id', Auth::user()->id)->where('completed', '=',  1)->orderBy('name')->get();
    $types = TaskType::all();
    $clients = Client::all();
    $states = State::all();
    return view('pages.dashboard.index', [
        'tasks' => $activeTasks,
        'expiredTasks' => $expiredTasks,
        'completedTasks' => $completedTasks,
        'types' => $types,
        'clients' => $clients,
        'states' => $states,
        'users' => $user,
    ]);

}

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
