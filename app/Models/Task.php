<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'description', 'expiration_date', 'user_id', 'completed', 'notCompleted', 'client_id', 'task_type_id', 'assigned_user_id'];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
    public function task_type()
    {
        return $this->hasOne(TaskType::class);
    }
}
