<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\State;
class StateDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {

            $states = ['Backlog', 'In progress', 'Internal Review', 'External Review', 'Bug', 'Completed'];
            foreach ($states as $state) {
                $newState = new State();
                $newState->state = $state;
                $newState->save();
            }

    }
}
