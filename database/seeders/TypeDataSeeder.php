<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TaskType;

class TypeDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $types = ['coding', 'meeting', 'reviewing', 'testing', 'updating'];
        foreach ($types as $type) {
            $newType = new TaskType();
            $newType->type = $type;
            $newType->save();
        }
    }
}
