<div class="p-2 bg-white w-60 flex flex-col hidden md:flex" id="sideNav">
    <nav>
        <a class="block text-gray-500 py-2.5 px-4 my-4 rounded transition duration-200 hover:bg-gradient-to-r hover:from-cyan-400 hover:to-cyan-300 hover:text-white" href="#">
            <i class="fas fa-home mr-2"></i>Home
        </a>
        <a class="block text-gray-500 py-2.5 px-4 my-4 rounded transition duration-200 hover:bg-gradient-to-r hover:from-cyan-400 hover:to-cyan-300 hover:text-white" href="{{ route('tasks.create') }}">
            <i class="fas fa-file-alt mr-2"></i>Create a New Task
        </a>
    </nav>

    <!-- Ítem de Cerrar Sesión -->
    <a class="block text-gray-500 py-2.5 px-4 my-2 rounded transition duration-200 hover:bg-gradient-to-r hover:from-cyan-400 hover:to-cyan-300 hover:text-white mt-auto" href="#">
        <i class="fas fa-sign-out-alt mr-2"></i>Cerrar sesión
    </a>
