<?php

use function Laravel\Folio\{middleware};
//use function Livewire\Volt\{state};

middleware(['auth', 'verified']);

?>


<x-layouts.dashboard>
    @if(Auth::user()->admin == 1)
    <a href="{{ route('clients.create') }}">Add a new Client</a>
    <table class="w-full text-sm table-auto">
        <thead>
            <tr class="text-sm leading-normal">
                <th class="px-4 py-2 text-sm font-bold uppercase border-b bg-grey-lightest text-grey-light border-grey-light">Edit</th>
                <th class="px-4 py-2 text-sm font-bold uppercase border-b bg-grey-lightest text-grey-light border-grey-light">Name</th>
                <th class="px-4 py-2 text-sm font-bold uppercase border-b bg-grey-lightest text-grey-light border-grey-light">Email</th>
                <th class="px-4 py-2 text-sm font-bold uppercase border-b bg-grey-lightest text-grey-light border-grey-light">Phone Number</th>
            </tr>
        </thead>
        <tbody>
            @foreach($clients as $client)
            <tr class="hover:bg-grey-lighter">

            {{-- <td class="px-4 py-2 border-b border-grey-light"><a href="{{ route('tasks.show', $task->id) }}">Show</a>
                <form method="POST" action="{{ route('completed', $task->id) }}">
                    @csrf
                    <button class="text-green-500 completed-button" type="submit">Completed</button>
                </form>
            </td> --}}
            <td class="px-4 py-2 border-b border-grey-light">    <a href="{{ route('clients.edit', $client->id) }}" class="bg-cyan-500 hover:bg-cyan-600 text-white font-semibold py-2 px-4 rounded">
                Edit Client
            </a>
            <form action="{{ route('clients.destroy', $client->id) }}" method="POST">
                @method('DELETE')
                @csrf
                <div class="px-4 py-2">
                    <button type="submit" data-task-name="{{ $client->name }}" class="bg-red-500 hover:bg-red-600 text-white font-semibold py-2 px-4 rounded">
                        Remove client
                    </button>
                </div>
            </form>
        </td>

            <td class="px-4 py-2 border-b border-grey-light">{{$client->name}}</td>
            <td class="px-4 py-2 border-b border-grey-light">{{$client->email}}</td>
            <td class="px-4 py-2 border-b border-grey-light">{{$client->phone_number}}</td>
         </tr>
        @endforeach

        </tbody>
        </table>

        @endif
</x-layouts.dashboard>
