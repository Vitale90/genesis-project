<?php
use function Laravel\Folio\{middleware};
//use function Livewire\Volt\{state};

middleware(['auth', 'verified']);
?>
<x-layouts.app>

<x-layouts.dashboard>

<form action="{{ route('tasks.store') }}" method="POST">
    @csrf
    <div class="relative z-0 w-full mb-6 group">
        <input type="text" name="name" id="name" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required />
        <label for="name" class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Type a name for your task</label>
    </div>
    <div class="grid md:grid-cols-2 md:gap-6">
      <div class="relative z-0 w-full mb-6 group">
          <input type="text" name="description" id="description" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required />
          <label for="description" class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Description</label>
      </div>
      <div class="relative z-0 w-full mb-6 group">
          <input type="date" name="expiration_date" id="expiration_date" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" " required />
          <label for="expiration_date" class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6">Expiration Date</label>
      </div>
    </div>
    <div class="grid md:grid-cols-2 md:gap-6">

    </div>
    <div class="relative z-0 w-full mb-6 group">
       <select name="task_type_id" id="task_type_id" required>
           <option value="">Add a Type of task</option>
           @foreach ($types as $type)
        <option value="{{$type->id}}">{{$type->type}}</option>
        @endforeach
       </select>
       <select name="client_id" id="client_id" required>
           <option value="">Add a Client</option>
           @foreach ($clients as $client)
       <option value="{{$client->id}}">{{$client->name}}</option>
       @endforeach
      </select>
      <select name="state_id" id="state_id" required>
          <option value="">Add State </option>
          @foreach ($states as $state)
       <option value="{{$state->id}}">{{$state->state}}</option>
       @endforeach
      </select>
      <select name="assigned_user_id" id="assigned_user_id" required>
          <option value="">Add Operator</option>
          @foreach ($users as $user)
       <option value="{{$user->id}}">{{$user->name}}</option>
       @endforeach
      </select>
    <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>
</form>



</x-layouts.dashboard>

</x-layouts.app>
