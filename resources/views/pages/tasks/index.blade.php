<?php

use function Laravel\Folio\{middleware};
//use function Livewire\Volt\{state};

middleware(['auth', 'verified']);

?>

<x-layouts.dashboard>

<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <style>
        @media (max-width: 768px) {
            .flex-wrap {
                display: flex;
                flex-wrap: wrap;
            }
            .section-small {
                width: 50%;
            }
        }
    </style>
</head>
<body>
<div class="flex flex-col h-screen bg-gray-100">
    {{-- Main content --}}
    <div class="flex flex-1">

        <!-- Área de contenido principal -->
        <div class="flex-1 p-4">

            <!-- Contenedor de las 4 secciones (disminuido para dispositivos pequeños) -->
            <div class="grid grid-cols-1 gap-4 p-2 mt-2 md:grid-cols-2">
                <!-- Sección 1 - Gráfica de Usuarios (disminuida para dispositivos

                <!-- Sección 1 - Gráfica de Usuarios -->
                    <div class="p-4 bg-white rounded-md">
                        <h2 class="pb-1 text-lg font-semibold text-gray-500">Workflow</h2>
                        <div class="my1-"></div> <!-- Espacio de separación -->
                        <div class="h-px mb-6 bg-gradient-to-r from-cyan-300 to-cyan-500"></div> <!-- Línea con gradiente -->
                        <div class="chart-container" style="position: relative; height:150px; width:100%">
                            <!-- El canvas para la gráfica -->
                            <canvas id="usersChart"></canvas>
                        </div>
                    </div>

                    {{-- Expiration table --}}
                    <div class="p-4 bg-white rounded-md">
                        <h2 class="pb-4 text-lg font-semibold text-gray-500">Expired</h2>
                        <div class="my-1"></div>
                        <table class="w-full text-sm table-auto">
                            <thead>
                                <tr class="text-sm leading-normal">
                                    <th class="px-4 py-2 text-sm font-bold text-left uppercase border-b bg-grey-light text-grey-light border-grey-light">Action</th>
                                    <th class="px-4 py-2 text-sm font-bold text-left uppercase border-b bg-grey-ligh text-grey-light border-grey-light">Name</th>
                                    <th class="px-4 py-2 text-sm font-bold text-left uppercase border-b bg-grey-light text-grey-light border-grey-light">description</th>
                                    <th class="px-4 py-2 text-sm font-bold text-left uppercase border-b bg-grey-light text-grey-light border-grey-light">Expiration Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($expiredTasks as $Etask)
                                @if($Etask->completed == 0)
                                    <tr class="hover:bg-grey-lighter">

                                        <td class="px-4 py-2 text-blue-900 border-b border-grey-light"><a href="{{ route('tasks.show', $Etask->id) }}"><i class="fa-solid fa-eye"></i></a>
                                            <form method="POST" action="{{ route('completed', $Etask->id) }}">
                                                @csrf
                                                <button class="mt-4 text-green-900 completed-button" type="submit"><i class="fa-solid fa-check"></i></button>
                                            </form>
                                        </td>
                                    <td class="px-4 py-2 text-left border-b border-grey-light">{{$Etask->name}}</td>
                                    <td class="px-4 py-2 text-left border-b border-grey-light">{{$Etask->description}}</td>
                                    <td class="px-4 py-2 text-left border-b border-grey-light">{{$Etask->expiration_date}}</td>
                                 </tr>
                                 @endif
                                @endforeach

                            </tbody>
                            </table>
                    </div>

                    <!-- Sección 3 - Tabla de Autorizaciones Pendientes (disminuida para dispositivos pequeños) -->
            <div class="p-4 bg-white rounded-md">
                <h2 class="pb-4 text-lg font-semibold text-gray-500">Your Tasks</h2>
                <div class="my-1"></div> <!-- Espacio de separación -->
                <div class="h-px mb-6 bg-gradient-to-r from-cyan-300 to-cyan-500"></div> <!-- Línea con gradiente -->
                <table class="w-full text-sm table-auto">
                    <thead>
                        <tr class="text-sm leading-normal">
                            <th class="px-4 py-2 text-sm font-bold text-left uppercase border-b bg-grey-light text-grey-light border-grey-light">Action</th>
                            <th class="px-4 py-2 text-sm font-bold text-left uppercase border-b bg-grey-light text-grey-light border-grey-light">Name</th>
                            <th class="px-4 py-2 text-sm font-bold text-left uppercase border-b bg-grey-light text-grey-light border-grey-light">description</th>
                            <th class="px-4 py-2 font-bold text-left uppercase border-b ptext-sm bg-grey-light text-grey-light border-grey-light">Expiration Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($tasks as $task)
                        @if($task->completed == 0)
                            <tr class="hover:bg-grey-lighter">

                            <td class="px-4 py-2 text-blue-900 border-b border-grey-light"><a href="{{ route('tasks.show', $task->id) }}"><i class="fa-solid fa-eye"></i></a>
                                <form method="POST" action="{{ route('completed', $task->id) }}">
                                    @csrf
                                    <button class="mt-4 text-green-900 completed-button" type="submit"><i class="fa-solid fa-check"></i></button>
                                </form>
                            </td>
                            <td class="px-4 py-2 text-left border-b border-grey-light">{{$task->name}}</td>
                            <td class="px-4 py-2 text-left border-b border-grey-light">{{$task->description}}</td>
                            <td class="px-4 py-2 text-left border-b border-grey-light">{{$task->expiration_date}}</td>



                         </tr>
                         @endif
                        @endforeach

                    </tbody>
                    </table>
                    <div class="mt-4 text-right">
                        <a class="px-4 py-2 font-semibold text-white rounded bg-cyan-500 hover:bg-cyan-600" href="{{ route('tasks.create') }}">
                            Add A New Task
                        </a>
                    </div>
                </div>

                <!-- Sección 4 - Tabla de Transacciones (disminuida para dispositivos pequeños) -->
                     <div class="p-4 mt-4 bg-white rounded-md">
    <h2 class="pb-4 text-lg font-semibold text-gray-500">Tasks completed</h2>
    <div class="my-1"></div> <!-- Espacio de separación -->
    <div class="h-px mb-6 bg-gradient-to-r from-cyan-300 to-cyan-500"></div> <!-- Línea con gradiente -->
    <table class="w-full text-sm table-auto">
        <thead>
            <tr class="text-sm leading-normal">
                <th class="px-4 py-2 text-sm font-bold text-left uppercase border-b bg-grey-lightest text-grey-light border-grey-light">Action</th>
                <th class="px-4 py-2 text-sm font-bold text-left uppercase border-b bg-grey-lightest text-grey-light border-grey-light">Name</th>
                <th class="px-4 py-2 text-sm font-bold text-left uppercase border-b bg-grey-lightest text-grey-light border-grey-light">Description</th>
                
            </tr>
        </thead>
        <tbody>
            @foreach($completedTasks as $Ctask)
                <tr class="hover:bg-grey-lighter">

                <td class="px-4 py-4 text-left text-blue-900 border-b border-grey-light"><a href="{{ route('tasks.show', $Ctask->id) }}"><i class="fa-solid fa-eye"></i></a></td>
                <td class="px-4 py-4 text-left border-b border-grey-light">{{$Ctask->name}}</td>
                <td class="px-4 py-4 text-left border-b border-grey-light">{{$Ctask->description}}</td>

             </tr>
            @endforeach

        </tbody>
    </table>
</div>


</div>
</div>
</div>


<!-- Script para las gráficas -->
<script>
    // Gráfica de Usuarios
    let usersChart = new Chart(document.getElementById('usersChart'), {
        type: 'doughnut',
        data: {
            labels: ['Active tasks', 'Expired tasks', 'Completed tasks'],
            datasets: [{
                data: [{{count($tasks)}}, {{count($expiredTasks)}}, {{count($completedTasks)}}],
                backgroundColor: ['#2b72cf', '#c93033', '#2bb32b'],
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                position: 'bottom' // Ubicar la leyenda debajo del círculo
            }
        }
    });

    // Gráfica de Comercios
    var commercesChart = new Chart(document.getElementById('commercesChart'), {
        type: 'doughnut',
        data: {
            labels: ['Nuevos', 'Registrados'],
            datasets: [{
                data: [60, 40],
                backgroundColor: ['#FEC500', '#8B8B8D'],
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                position: 'bottom' // Ubicar la leyenda debajo del círculo
            }
        }
    });

    // Agregar lógica para mostrar/ocultar la navegación lateral al hacer clic en el ícono de menú
    const menuBtn = document.getElementById('menuBtn');
    const sideNav = document.getElementById('sideNav');

    menuBtn.addEventListener('click', () => {
        sideNav.classList.toggle('hidden'); // Agrega o quita la clase 'hidden' para mostrar u ocultar la navegación lateral
    });
</script>
</body>
{{-- @volt('dashboard')
<div class="h-full py-12">
    <div class="h-full mx-auto max-w-7xl sm:px-6 lg:px-8">

        <div class="relative min-h-[500px] w-full h-full">
        </div>

    </div>
</div>
@endvolt --}}
</x-layouts.dashboard>
