<?php

use function Laravel\Folio\{middleware};
//use function Livewire\Volt\{state};

middleware(['auth', 'verified']);

?>

<x-layouts.dashboard>
<div class="container">
    <div class="title">
        <h2>{{$task->name}}</h2>
    </div>
    <div class="description">
        <p>{{$task->description}}</p>
    </div>
    <div class="date">
        <h3>{{$task->expiration_date}}</h3>
    </div>
    <div class="type">
        <h3>{{$types[$task->task_type_id -1]->type}}</h3>
    </div>
    <div class="client">
        <h3>{{$clients[$task->client_id -1]->name}}</h3>
    </div>
    <div class="operator">
        <h3>{{$users[$task->assigned_user_id -1]->name}}</h3>

    </div>
</div>
<div class="crud-buttons">

    <a href="{{ route('tasks.edit', $task->id) }}" class="px-4 py-2 font-semibold rounded text-dark bg-cyan-500 hover:bg-cyan-600">
        Edit Task
    </a>
    <form action="{{ route('tasks.destroy', $task->id) }}" method="POST">
        @method('DELETE')
        @csrf
        <div class="mt-5">
            <button type="submit" data-task-name="{{ $task->name }}" class="px-4 py-2 font-semibold bg-red-500 rounded text-dark hover:bg-red-600">
                Remove Task
            </button>
        </div>
    </form>
    <form method="POST" action="{{ route('notCompleted', $task->id) }}">
        @csrf
        {{-- <button class="text-dark-500 completed-button" type="submit">Not Completed Yet</button> --}}
        <button class="px-2 py-2 my-3 font-semibold bg-orange-500 rounded" type="submit">Not Completed Yet</button>

    </form>
</div>

</x-layouts.dashboard>
