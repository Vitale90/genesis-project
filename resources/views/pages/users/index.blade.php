<?php

use function Laravel\Folio\{middleware};
//use function Livewire\Volt\{state};

middleware(['auth', 'verified']);

?>


<x-layouts.dashboard>
    @if(Auth::user()->admin == 1)
    <table class="w-full text-sm table-auto">
        <thead>
            <tr class="text-sm leading-normal">
                <th class="px-4 py-2 text-sm font-bold uppercase border-b bg-grey-lightest text-grey-light border-grey-light">Role</th>
                <th class="px-4 py-2 text-sm font-bold uppercase border-b bg-grey-lightest text-grey-light border-grey-light">Name</th>
                <th class="px-4 py-2 text-sm font-bold uppercase border-b bg-grey-lightest text-grey-light border-grey-light">Email</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
        <tr>
            @if($user->admin == 1)
            <td class="px-4 py-2 border-b border-grey-light">Admin</td>
            @endif
            @if($user->admin != 1)
            <td class="px-4 py-2 border-b border-grey-light">User</td>
            @endif
            <td class="px-4 py-2 border-b border-grey-light">{{$user->name}}</td>
            <td class="px-4 py-2 border-b border-grey-light">{{$user->email}}</td>

         </tr>
        @endforeach

        </tbody>
        </table>

        @endif
</x-layouts.dashboard>
