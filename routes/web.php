<?php

use App\Http\Controllers\Auth\EmailVerificationController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\ClientController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\TypeTasksController;
use App\Http\Controllers\UserController;
use App\Models\TaskType;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('home', '/tasks')->name('home');

Route::middleware('auth')->group(function () {
    Route::get('email/verify/{id}/{hash}', EmailVerificationController::class)
        ->middleware('signed')
        ->name('verification.verify');
    Route::post('logout', LogoutController::class)
        ->name('logout');

    Route::resource('tasks', TaskController::class);
    Route::get('/tasks', [TaskController::class, 'index'])->name('tasks.index');
    Route::post('/completed/{task}', [TaskController::class, 'completed'])->name('completed');
    Route::post('/notCompleted/{task}', [TaskController::class, 'notCompleted'])->name('notCompleted');
    Route::resource('clients', ClientController::class);
    Route::get('/clients', [ClientController::class, 'index'])->name('clients.index');
    Route::get('/users', [UserController::class, 'index'])->name('users.index');
    Route::get('/dashboard', [UserController::class, 'show'])->name('users.show');
});
